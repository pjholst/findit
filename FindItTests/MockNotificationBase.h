//
//  MockNotificationBase.h
//  FindIt
//
//  Created by Philip Holst on 10/5/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MockNotificationBase : NSObject

@property BOOL wasHit;

- (void)mockNotification:(NSNotification*)notification;
- (void)tearDown;
@end
