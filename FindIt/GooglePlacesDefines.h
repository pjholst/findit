//
//  GooglePlacesDefines.h
//  FindIt
//
//  Created by Philip Holst on 10/9/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#ifndef FindIt_GooglePlacesDefines_h
#define FindIt_GooglePlacesDefines_h

#import <Foundation/Foundation.h>

// String Defines
static NSString* const kGoogleURLStr = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=%f&types=%@&key=%@";
static NSString* const kGoogleURLMoreStr = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=%@&key=%@";
static NSString* const kGoogleStatusStr = @"status";
static NSString* const kGoogleStatusOK = @"OK";
static NSString* const kGoogleResults = @"results";
static NSString* const kGoogleZeroResults = @"ZERO_RESULTS";
static NSString* const kGoogleOverLimit = @"OVER_QUERY_LIMIT";
static NSString* const kGoogleRequestDenied = @"REQUEST_DENIED";
static NSString* const kGoogleInvalidRquest = @"INVALID_REQUEST";

// Keys
static NSString* const kGoogleKeyID = @"place_id";
static NSString* const kGoogleKeyGeo = @"geometry";
static NSString* const kGoogleKeyLocation = @"location";
static NSString* const kGoogleKeyLat = @"lat";
static NSString* const kGoogleKeyLng = @"lng";
static NSString* const kGoogleKeyRating = @"rating";
static NSString* const kGoogleKeyName = @"name";
static NSString* const kGoogleKeyIcon = @"icon";
static NSString* const kGoogleKeyVicinity = @"vicinity";
static NSString* const kGoogleKeyPageToken = @"next_page_token";

#endif

