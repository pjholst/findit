//
//  GoogleMapView.h
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

#import "GeneralMapViewProtocol.h"

@class GMSMapView;
@class GMSCircle;

@interface GoogleMapView : NSObject <GeneralMapViewProtocol, GMSMapViewDelegate>
{
    @private
    GMSMapView *mapView_;
    GMSCircle *rangeCircle_;
    GMSMarker *userPositionMarker_;
    NSArray *markerArray_;
}

- (id)initWithFrameForMap:(CGRect)frame;

@end
