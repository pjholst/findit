//
//  GooglePlacesInterfaceTests.m
//  FindIt
//
//  Created by Philip Holst on 10/9/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "GooglePlacesDefines.h"
#import "GooglePlacesInterface.h"
#import "LocationInterface.h"
#import "PlaceData.h"
#import "QueryData.h"
#import "WebServiceInterface.h"

@interface GooglePlacesInterfaceTests : XCTestCase
{
    NSArray* includeStrArray;
    NSArray* notIncludeStrArray;
}

- (void)testNSURL:(NSURL*)theURL;
- (NSDictionary*)buildTestDictionary;
- (void)parsePlaceData:(NSDictionary*)placeData;

@end

@implementation GooglePlacesInterfaceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testStartQueryForLocationTypeNoMoreResultsKey
{
    // Mock Setup
    id mockLocationInterface = OCMClassMock([LocationInterface class]);
    OCMStub(ClassMethod([mockLocationInterface sharedInterface])).andReturn(mockLocationInterface);
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(100, 100);
    OCMStub([mockLocationInterface currentCoordinate]).andReturn(coordinate);
    
    id mockWebServiceInterface = OCMClassMock([WebServiceInterface class]);
    OCMStub(ClassMethod([mockWebServiceInterface sharedInterface])).andReturn(mockWebServiceInterface);
    OCMStub([mockWebServiceInterface processHTTPRequest:[OCMArg any]]).andCall(self, @selector(testNSURL:));
    
    // There is no need to mock QueryData for this test.
    
    includeStrArray = @[@"radius", @"types", @"key"];
    notIncludeStrArray = @[@"pagetoken"];
    
    // The Test
    GooglePlacesInterface * testInterace = [[GooglePlacesInterface alloc] init];
    [testInterace startQueryForLocationType:kFlorist withRadius:4000];
    
    
    // Verify Mock Objects
    OCMVerify([mockLocationInterface sharedInterface]);
    OCMVerify([mockLocationInterface currentCoordinate]);
    
    OCMVerify([mockWebServiceInterface sharedInterface]);
    // We don't need to verify mockWebInterface processHTTPRequest, as the stub swizzles the implementation.
}

- (void)testStartQueryForLocationTypeWithMoreResultsKey
{
    // Mock Setup
    id mockLocationInterface = OCMClassMock([LocationInterface class]);
    OCMStub(ClassMethod([mockLocationInterface sharedInterface])).andReturn(mockLocationInterface);
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(100, 100);
    OCMStub([mockLocationInterface currentCoordinate]).andReturn(coordinate);
    
    id mockWebServiceInterface = OCMClassMock([WebServiceInterface class]);
    OCMStub(ClassMethod([mockWebServiceInterface sharedInterface])).andReturn(mockWebServiceInterface);
    OCMStub([mockWebServiceInterface processHTTPRequest:[OCMArg any]]).andCall(self, @selector(testNSURL:));
    
    id mockQueryData = OCMClassMock([QueryData class]);
    OCMStub(ClassMethod([mockQueryData sharedData])).andReturn(mockQueryData);
    OCMStub([mockQueryData moreResultsKey]).andReturn(@"12345");
    
    includeStrArray = @[@"pagetoken", @"key"];
    notIncludeStrArray = @[@"radius", @"types"];;
    
    // The Test
    GooglePlacesInterface * testInterace = [[GooglePlacesInterface alloc] init];
    [testInterace startQueryForLocationType:kFlorist withRadius:4000];
    
    
    // Verify Mock Objects
    OCMVerify([mockLocationInterface sharedInterface]);
    OCMVerify([mockLocationInterface currentCoordinate]);
    
    OCMVerify([mockQueryData sharedData]);
    OCMVerify([mockQueryData moreResultsKey]);
    
    OCMVerify([mockWebServiceInterface sharedInterface]);
    // We don't need to verify mockWebInterface processHTTPRequest, as the stub swizzles the implementation.
}

- (void)testParseData
{
    id mockQueryData = OCMClassMock([QueryData class]);
    OCMStub(ClassMethod([mockQueryData sharedData])).andReturn(mockQueryData);
    OCMStub([mockQueryData addData:[OCMArg any]]);
    
    NSDictionary* testData = [self buildTestDictionary];
    GooglePlacesInterface * testInterface = [[GooglePlacesInterface alloc] init];
    [testInterface parseData:@[testData]];
}

#pragma mark - Helpers

- (void)testNSURL:(NSURL *)theURL
{
    NSString* urlStr = [theURL absoluteString];
    for (NSString* partStr in includeStrArray)
    {
        XCTAssertNotEqual([urlStr rangeOfString:partStr].location, NSNotFound);
    }
    
    for (NSString* partStr in notIncludeStrArray)
    {
        XCTAssertEqual([urlStr rangeOfString:partStr].location, NSNotFound);
    }
}

- (NSDictionary*)buildTestDictionary
{
    NSMutableDictionary* testDictionary = [[NSMutableDictionary alloc] init];
    
    // ID
    [testDictionary setObject:@"12345" forKey:kGoogleKeyID];
    
    // Location
    NSDictionary* location = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:100], kGoogleKeyLat, [NSNumber numberWithDouble:100], kGoogleKeyLng, nil];
    NSDictionary* geo = [NSDictionary dictionaryWithObjectsAndKeys:location, kGoogleKeyLocation, nil];
    [testDictionary setObject:geo forKey:kGoogleKeyGeo];
    
    // Rating
    [testDictionary setObject:[NSNumber numberWithFloat:3.5f] forKey:kGoogleKeyRating];
    
    // Name
    [testDictionary setObject:@"TestName" forKey:kGoogleKeyName];
    
    // Icon
    [testDictionary setObject:@"testURL" forKey:kGoogleKeyIcon];
    
    // Address
    [testDictionary setObject:@"123 Test Street" forKey:kGoogleKeyVicinity];
    
    return testDictionary;
}

- (void)parsePlaceData:(NSDictionary *)placeData
{
    PlaceData* data = [placeData objectForKey:@"12345"];
    
    XCTAssertEqual(data.coordinate.latitude, 100);
    XCTAssertEqual(data.coordinate.longitude, 100);
    XCTAssertEqual(data.name, @"TestName");
    XCTAssertEqual(data.imageIcon, [NSURL URLWithString:@"testUrl"]);
    XCTAssertEqual(data.rating, 3.5);
}

@end
