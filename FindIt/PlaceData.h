//
//  PlaceData.h
//  FindIt
//
//  Created by Philip Holst on 10/5/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PlaceData : NSObject

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) double rating;
@property (copy, nonatomic) NSString* name;
@property (strong, nonatomic) NSURL* imageIcon;
@property (copy, nonatomic) NSString* address;

@end
