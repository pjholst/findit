//
//  GooglePlacesInterface.h
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceInterfaceProtocol.h"

@interface GooglePlacesInterface : NSObject <WebServiceInterfaceProtocol>

- (void)parseData:(NSArray *)dataArray;
- (void)showAlert:(NSString *)title message:(NSString*)message cancelButton:(NSString*)cancelTitle;

@end
