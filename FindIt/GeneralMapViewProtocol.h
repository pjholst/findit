//
//  GeneralMapViewProtocol.h
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#ifndef FindIt_GeneralMapViewProtocol_h
#define FindIt_GeneralMapViewProtocol_h

#import <MapKit/MapKit.h>

@protocol GeneralMapViewProtocol <NSObject>

@required
- (UIView*)getView;
- (void)resetFrame:(CGRect)frame;
// Setting Position and Zoom
- (void)setUserPosiiton:(CLLocationCoordinate2D)coordinate;
- (void)setCenterPosition:(CLLocationCoordinate2D)coordinate;
- (void)setZoomForRadius:(double)radius aroundCoordinate:(CLLocationCoordinate2D)coordinate;

// Managing Markers
- (void)addMarkersFromPlaceData;
- (void)clearMarkers;

@optional
// Range Ring Methods
- (void)addRangeRing:(double)radius aroundCoordinate:(CLLocationCoordinate2D)coordinate;
- (void)changeRangeRingColor:(UIColor*)color;
- (void)removeRangeRing;

@end

#endif
