//
//  UserTypeConverter.m
//  FindIt
//
//  Created by Philip Holst on 10/7/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "UserTypeConverter.h"

#import "UserDefaultPrefsDefines.h"

@implementation UserTypeConverter

+ (NSString*)convertWebServiceToString:(WebServiceType)webService
{
    NSString* retStr = nil;
    
    switch (webService) {
        case kGoogleService:
            retStr = kGoogleServiceString;
            break;
            
        case kYelpService:
            retStr = kYelpServiceString;
            break;
            
        default:
            [NSException raise:NSGenericException format:@"Unexpected WebService."];
    }
    
    return  retStr;
}

+ (WebServiceType)convertStringtoWebServiceType:(NSString *)string
{
    WebServiceType webService = kGoogleService;
    
    if ([string isEqualToString:kGoogleServiceString])
        webService = kGoogleService;
    else if ([string isEqualToString:kYelpServiceString])
        webService = kYelpService;
    else
        [NSException raise:NSGenericException format:@"Unexpected String Value."];
    
    return webService;
}

+ (NSString*)convertLocationTypeToString:(LocationType)location forService:(WebServiceType)webService
{
    NSString* resultStr = nil;
    switch (webService) {
        case kGoogleService:
        {
            switch (location) {
                case kFlorist:
                    resultStr = kGoogleLocationFlorist;
                    break;
                    
                case kRestaurant:
                    resultStr = kGoogleLocationRestaurant;
                    break;
                    
                case kGasStation:
                    resultStr = kGoogleLocationGasStation;
                    break;
                    
                default:
                    [NSException raise:NSGenericException format:@"Unexpected LocationType."];
            }
            break;
        }
        case kYelpService:
        {
            switch (location) {
                case kFlorist:
                    resultStr = kYelpLocationFlorist;
                    break;
                    
                case kRestaurant:
                    resultStr = kYelpLocationRestaurant;
                    break;
                    
                case kGasStation:
                    resultStr = kYelpLocationGasStation;
                    break;
                    
                default:
                    [NSException raise:NSGenericException format:@"Unexpected LocationType."];
            }
            break;
        }
        case kLastService:
            [NSException raise:NSGenericException format:@"Unexpected Webservice."];
            break;
    }
    
    return resultStr;
}

+ (LocationType)convertStringToLocationType:(NSString *)string forService:(WebServiceType)webService
{
    LocationType returnVal = kFlorist;
    
    switch (webService) {
        case kGoogleService:
        {
            if ([string isEqualToString:kGoogleLocationFlorist])
                returnVal = kFlorist;
            else if ([string isEqualToString:kGoogleLocationRestaurant])
                returnVal = kRestaurant;
            else if ([string isEqualToString:kGoogleLocationGasStation])
                returnVal = kGasStation;
            else
                [NSException raise:NSGenericException format:@"Unexpected String Value."];
            
            break;
        }
            
        case kYelpService:
        {
            if ([string isEqualToString:kYelpLocationFlorist])
                returnVal = kFlorist;
            else if ([string isEqualToString:kYelpLocationRestaurant])
                returnVal = kRestaurant;
            else if ([string isEqualToString:kYelpLocationGasStation])
                returnVal = kGasStation;
            else
                [NSException raise:NSGenericException format:@"Unexpected String Value."];
            
            break;
        }
            
        case kLastService:
            [NSException raise:NSGenericException format:@"Unexpected Web Service."];
            break;
    }
    
    return returnVal;
}

+ (NSString*)getDefaultLocationTypeString
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:kDefaultLocationType];
}

+ (WebServiceType)getDefaultWebService
{
    return [UserTypeConverter convertStringtoWebServiceType:[[NSUserDefaults standardUserDefaults] stringForKey:kDefaultService]];
}

+ (LocationType)getDefaultLocationType
{
    return [UserTypeConverter convertStringToLocationType:[UserTypeConverter getDefaultLocationTypeString]
                                               forService:[UserTypeConverter getDefaultWebService]];
}

+ (NSString*)getLocationDisplayName:(LocationType)location
{
    NSString* returnStr = nil;
    
    switch (location) {
        case kFlorist:
            returnStr = NSLocalizedString(@"Florist", nil);
            break;
            
        case kRestaurant:
            returnStr = NSLocalizedString(@"Restaurant", nil);
            break;
            
        case kGasStation:
            returnStr = NSLocalizedString(@"Gas Station", nil);
            break;
            
        default:
            // No Op
            break;
    }
    
    return returnStr;
}

@end
