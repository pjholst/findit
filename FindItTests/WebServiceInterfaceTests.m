//
//  WebServiceInterfaceTests.m
//  FindIt
//
//  Created by Philip Holst on 10/9/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "GooglePlacesInterface.h"
#import "UserDefaultPrefsDefines.h"
#import "UserTypeConverter.h"
#import "WebServiceInterface.h"
#import "WebServiceDefines.h"

@interface WebServiceInterfaceTests : XCTestCase

@end

@implementation WebServiceInterfaceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testGetWebServiceClass
{
    WebServiceInterface *interface = [[WebServiceInterface alloc] init];
    
    for (WebServiceType webType = 0; webType <= kLastService; webType++)
    {
        interface.webType = webType;
        switch (webType) {
            case kGoogleService:
            {
                // Not sure why, but these must be called outside of the XCTest Assert for the match to work.
                Class resultClass = [GooglePlacesInterface class];
                Class testClass = [GooglePlacesInterface class];
                XCTAssertEqual(testClass, resultClass);
            }
                break;
                
            case kYelpService:
                XCTAssertThrows([interface getWebServiceClass]);
                break;
                
            default:
                XCTAssertThrows([interface getWebServiceClass]);
                break;
        }
    }
}

- (void)testStartWebQuery
{
    // Setup Mocks
    id responderMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([responderMock standardUserDefaults]).andReturn(responderMock);
    OCMStub([responderMock stringForKey:kDefaultService]).andReturn(kGoogleServiceString);
    OCMExpect([responderMock stringForKey:[OCMArg any]]).andReturn(@"test");
    OCMExpect([responderMock doubleForKey:[OCMArg any]]).andReturn(4000);
    
    id typeConverterMock = OCMClassMock([UserTypeConverter class]);
    OCMStub([typeConverterMock convertStringToLocationType:[OCMArg any] forService:kGoogleService]).andReturn(kFlorist);
    
    id googleInterfaceMock = OCMClassMock([GooglePlacesInterface class]);
    OCMStub([googleInterfaceMock startQueryForLocationType:kFlorist withRadius:4000]);
    
    // Run Test
    WebServiceInterface* testInterface = [[WebServiceInterface alloc] init];
    testInterface.webType = kGoogleService;
    testInterface.delegate = googleInterfaceMock;
    [testInterface startWebQuery];
    
    // Verify Mocks
    OCMVerify([responderMock stringForKey:[OCMArg any]]);
    OCMVerify([responderMock doubleForKey:[OCMArg any]]);
    OCMVerify([typeConverterMock convertStringToLocationType:[OCMArg any] forService:kGoogleService]);
    OCMVerify([googleInterfaceMock startQueryForLocationType:kFlorist withRadius:4000]);
}

@end
