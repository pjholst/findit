//
//  LocationPicker.m
//  FindIt
//
//  Created by Philip Holst on 10/7/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "LocationPicker.h"

#import "NotificationDefines.h"
#import "UserDefaultPrefsDefines.h"
#import "UserTypeConverter.h"

@implementation LocationPicker

// Setup the view's controls based on user prefs.
- (void)setupView
{
    // Location Picker
    [self.locationPicker selectRow:[UserTypeConverter getDefaultLocationType]
                                          inComponent:0
                                             animated:NO];
    
    // Range slider and label.
    NSNumber* radiusNumber = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultRadius];
    self.rangeSlider.value = [radiusNumber floatValue] * METERS_TO_MILES;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setRoundingMode:NSNumberFormatterRoundHalfEven];
    [formatter setMaximumFractionDigits:1];
    self.rangeLabel.text = [NSString stringWithFormat:@"%@ Miles",
                            [formatter stringFromNumber:[NSNumber numberWithDouble:[radiusNumber floatValue] * METERS_TO_MILES]]];
    
    // Range Ring
    self.rangeRingSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:kDefaultRangeRing];
}

// Set the preferences based on changes in the UI. Also sends notification signals to any listeners that require changes.
- (void)setPrefsOnClose
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    // Location Picker
    LocationType currentType = [UserTypeConverter getDefaultLocationType];
    if (currentType != [self.locationPicker selectedRowInComponent:0])
    {
        LocationType newLocationType = (LocationType)[self.locationPicker selectedRowInComponent:0];
        [userDefaults setObject:[UserTypeConverter convertLocationTypeToString:newLocationType
                                                                    forService:[UserTypeConverter getDefaultWebService]]
                         forKey:kDefaultLocationType];
        
        [defaultCenter postNotificationName:LocationTypePrefNotification object:nil];
    }
    
    // Range Slider
    BOOL rangeChanged = NO;
    NSNumber* currentRadius = [userDefaults objectForKey:kDefaultRadius];
    if ([currentRadius floatValue] * METERS_TO_MILES != self.rangeSlider.value)
    {
        [userDefaults setDouble:self.rangeSlider.value * MILES_TO_METER forKey:kDefaultRadius];
        [defaultCenter postNotificationName:RangePrefNotification object:nil];
        rangeChanged = YES;
    }
    
    // Range Ring
    // If the range changed, the ring will need to be notified, even if the pref did not change.
    if (rangeChanged || self.rangeRingSwitch.on != [userDefaults boolForKey:kDefaultRangeRing])
    {
        [userDefaults setBool:self.rangeRingSwitch.on forKey:kDefaultRangeRing];
        
        // Pass a flag that tells the reciever if the range changed.
        NSMutableDictionary* notificationDictionary = [[NSMutableDictionary alloc] init];
        [notificationDictionary setObject:[NSNumber numberWithBool:rangeChanged] forKey:kRangeChangeKey];
        [defaultCenter postNotificationName:RangeRingPrefNotification object:nil userInfo:notificationDictionary];
    }
}

- (IBAction)rangerSliderUpdated:(id)sender
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setRoundingMode:NSNumberFormatterRoundHalfEven];
    [formatter setMaximumFractionDigits:1];
    self.rangeLabel.text = [NSString stringWithFormat:@"%@ Miles",
                            [formatter stringFromNumber:[NSNumber numberWithFloat:self.rangeSlider.value]]];
}

@end
