//
//  GooglePlacesInterface.m
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "GooglePlacesInterface.h"

#import "GoogleAPIKey.h"
#import "GooglePlacesDefines.h"
#import "LocationInterface.h"
#import "PlaceData.h"
#import "QueryData.h"
#import "UserTypeConverter.h"
#import "WebServiceDefines.h"
#import "WebServiceInterface.h"

@implementation GooglePlacesInterface

- (void)startQueryForLocationType:(LocationType)locationType withRadius:(double)radius
{
    NSString *googleType = [UserTypeConverter convertLocationTypeToString:locationType forService:kGoogleService];
    CLLocationCoordinate2D coordinate = [[LocationInterface sharedInterface] currentCoordinate];
    
    NSString *urlStr = nil;
    // We have not done a query for this location before, so lets start a new one.
    if ([[QueryData sharedData] moreResultsKey] == nil)
    {
        urlStr = [NSString stringWithFormat:kGoogleURLStr,
                  coordinate.latitude,
                  coordinate.longitude,
                  radius,
                  googleType,
                  kGooglePlacesKey];
    }
    else
    {
        urlStr = [NSString stringWithFormat:kGoogleURLMoreStr,
                  [[QueryData sharedData] moreResultsKey],
                  kGooglePlacesKey];
    }
    
    //Formulate the string as a URL object.
    NSURL *url = [NSURL URLWithString:urlStr];
    [[WebServiceInterface sharedInterface] processHTTPRequest:url];
}

// Not under test. Needs refactoring.
- (BOOL)processResponseData:(NSData *)data
{
    BOOL returnCode = NO;
    
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:NULL];
    NSString* statusStr = [dataDictionary objectForKey:kGoogleStatusStr];
    if ([statusStr isEqualToString:kGoogleStatusOK])
    {
        NSArray* dataArray = [dataDictionary objectForKey:kGoogleResults];
        [self parseData:dataArray];
        NSString* moreResultsKey = [dataDictionary objectForKey:kGoogleKeyPageToken];
        if (moreResultsKey)
            [[QueryData sharedData] setMoreResultsKey:moreResultsKey];
        else
            [[QueryData sharedData] setMoreResultsKey:nil];
        
        returnCode = YES;
        
    }
    else if ([statusStr isEqualToString:kGoogleZeroResults])
    {
        [self showAlert:NSLocalizedString(@"Results", nil)
                message:NSLocalizedString(@"No More Results", nil)
           cancelButton:NSLocalizedString(@"OK", nil)];
    }
    else if ([statusStr isEqualToString:kGoogleOverLimit])
    {
        [self showAlert:NSLocalizedString(@"Results", nil)
                message:NSLocalizedString(@"Over Data Limit", nil)
           cancelButton:NSLocalizedString(@"OK", nil)];
    }
    else if ([statusStr isEqualToString:kGoogleRequestDenied])
    {
        [self showAlert:NSLocalizedString(@"Results", nil)
                message:NSLocalizedString(@"Request Denied", nil)
           cancelButton:NSLocalizedString(@"OK", nil)];
    }
    else if ([statusStr isEqualToString:kGoogleInvalidRquest])
    {
        // Alert message not required in this case. This can happen
        // if you have tried to go past the last page of results.
        [[QueryData sharedData] setMoreResultsKey:nil];
        NSLog(@"Google: Invalid Request");
    }
    else
    {
        [self showAlert:NSLocalizedString(@"Results", nil)
                message:NSLocalizedString(@"Unknown Error", nil)
           cancelButton:NSLocalizedString(@"OK", nil)];
    }
    
    return returnCode;
}

- (void)processError:(NSError *)error
{
    NSString* errorDescription = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
    
    [self showAlert:NSLocalizedString(@"Alert", nil)
            message:errorDescription
       cancelButton:NSLocalizedString(@"OK", nil)];
}

- (void)parseData:(NSArray *)dataArray
{
    NSMutableDictionary* tempDictionary = [[NSMutableDictionary alloc] init];
    for (NSDictionary* dictionary in dataArray)
    {
        // id
        PlaceData* placeData = [[PlaceData alloc] init];
        NSString* placeID = [dictionary objectForKey:kGoogleKeyID];
        
        // Coordinate
        NSDictionary* geo = [dictionary objectForKey:kGoogleKeyGeo];
        NSDictionary* location = [geo objectForKey:kGoogleKeyLocation];
        NSNumber* latNumber = [location objectForKey:kGoogleKeyLat];
        NSNumber* lngNumber = [location objectForKey:kGoogleKeyLng];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([latNumber doubleValue], [lngNumber doubleValue]);
        placeData.coordinate = coordinate;
        
        // Rating
        NSNumber* ratingNum = [dictionary objectForKey:kGoogleKeyRating];
        placeData.rating = [ratingNum floatValue];
        
        // Name
        placeData.name = [dictionary objectForKey:kGoogleKeyName];
        
        // Icon
        NSString* iconStr = [dictionary objectForKey:kGoogleKeyIcon];
        placeData.imageIcon = [NSURL URLWithString:iconStr];
        
        // Address
        placeData.address = [dictionary objectForKey:kGoogleKeyVicinity];
        
        [tempDictionary setObject:placeData forKey:placeID];
        
        
    }
    
    [[QueryData sharedData] addData:tempDictionary];
}

- (void)showAlert:(NSString *)title message:(NSString *)message cancelButton:(NSString *)cancelTitle
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelTitle otherButtonTitles:nil];
        [alertView show];
    });
}

@end
