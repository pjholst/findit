//
//  WebServiceInterface.h
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WebServiceDefines.h"
#import "WebServiceInterfaceProtocol.h"

//
// A general purpose adapter for any possible webservices to work through.
// Implemented through a singleton.
//
@interface WebServiceInterface : NSObject

@property (nonatomic) WebServiceType webType;
@property (strong, nonatomic) id<WebServiceInterfaceProtocol> delegate;
@property (strong, atomic) NSOperationQueue* queue;

+ (id)sharedInterface;

- (Class)getWebServiceClass;

- (void)startWebQuery;
- (void)processHTTPRequest:(NSURL*)url; // Asynchronous call
- (NSData*)fetchDataFromURL:(NSURL*)url; // Synchronous call

@end
