//
//  GeneralMapViewTests.m
//  FindIt
//
//  Created by Philip Holst on 10/9/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "GeneralMapView.h"
#import "GeneralMapViewProtocol.h"
#import "LocationInterface.h"
#import "NotificationDefines.h"

@interface GeneralMapViewTests : XCTestCase

@end

@implementation GeneralMapViewTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRangeRingPrefChangedRingChangedRingOn
{
    GeneralMapView* testMapView = [[GeneralMapView alloc] init];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(100, 100);
    
    id mockDefaults = OCMClassMock([NSUserDefaults class]);
    OCMStub([mockDefaults standardUserDefaults]).andReturn(mockDefaults);
    OCMStub([mockDefaults boolForKey:[OCMArg any]]).andReturn(YES);
    
    id mockLocation = OCMClassMock([LocationInterface class]);
    OCMStub([mockLocation sharedInterface]).andReturn(mockLocation);
    OCMStub([mockLocation hasLocation]).andReturn(YES);
    OCMStub([mockLocation currentCoordinate]).andReturn(coordinate);
    
    id testView = OCMPartialMock(testMapView);
    OCMStub([testView removeRangeRing]);
    OCMStub([testView addRangeRing:4000 atCoordinate:coordinate]);
    
    NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], kRangeChangeKey, nil];
    NSNotification* notification = [[NSNotification alloc] initWithName:@"testNotification" object:self userInfo:dictionary];
    
    [testView rangeRingPrefChanged:notification];
    
    OCMVerify([mockDefaults standardUserDefaults]);
    OCMVerify([mockDefaults boolForKey:[OCMArg any]]);
    
    OCMVerify([mockLocation sharedInterface]);
    OCMVerify([mockLocation hasLocation]);
    OCMVerify([mockLocation currentCoordinate]);
    
    OCMVerify([testView removeRangeRing]);
    OCMVerify([testView addRangeRing:4000 atCoordinate:coordinate]);
}


@end
