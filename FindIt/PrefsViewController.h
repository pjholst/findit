//
//  PrefsViewController.h
//  FindIt
//
//  Created by Philip Holst on 10/6/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceDefines.h"

@interface PrefsViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView* webServicePicker;
@property (weak, nonatomic) IBOutlet UISlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *rangeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *rangeRingSwitch;

@property (strong, nonatomic) NSArray* webServiceNames;

- (NSString*)getDisplayName:(WebServiceType)service;
- (void)setupWebServiceNames;
- (IBAction)rangerSliderUpdated:(id)sender;

@end
