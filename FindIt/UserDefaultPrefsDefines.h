//
//  UserDefaultPrefsDefines.h
//  FindIt
//
//  Created by Philip Holst on 10/6/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#ifndef FindIt_UserDefaultPrefsDefines_h
#define FindIt_UserDefaultPrefsDefines_h

#import <Foundation/Foundation.h>
#import "WebServiceDefines.h"

#define METERS_TO_MILES 0.000621371f
#define MILES_TO_METER 1609.34f

// Keys
static NSString * const kDefaultService         = @"DefaultService";
static NSString * const kDefaultLocationType    = @"DefaultLocationType";
static NSString * const kDefaultRadius          = @"DefaultRadius";
static NSString * const kDefaultRangeRing       = @"DefaultRadiusRing";

// Default values that are not elsewhere defined.
NSNumber * defaultRadius;
NSNumber * defaultRing;
__attribute__((constructor))
static void InitGlobalNumber()
{
    defaultRadius = [NSNumber numberWithDouble:4000];
    defaultRing = [NSNumber numberWithBool:NO];
}

NSDictionary * defaultPrefs;
__attribute__((constructor))
static void InitGlobalDictionary()
{
    defaultPrefs =[NSDictionary dictionaryWithObjectsAndKeys:kGoogleServiceString, kDefaultService,
                   kGoogleLocationFlorist, kDefaultLocationType,
                   defaultRadius, kDefaultRadius,
                   defaultRing, kDefaultRangeRing,
                   nil];
}


#endif
