//
//  QueryDataTests.m
//  FindIt
//
//  Created by Philip Holst on 10/5/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "QueryData.h"

@interface QueryDataTests : XCTestCase
{
    QueryData* testData;
}

@end

@implementation QueryDataTests

- (void)setUp {
    [super setUp];
    testData = [[QueryData alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAddData
{
    NSString* key = @"testKey";
    NSDictionary* testDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:15], key, nil];
    [testData addData:testDictionary]; // With a random number
    NSNumber* testNumber = [testData.queryResults objectForKey:key];
    XCTAssertEqual(15, [testNumber integerValue]);
}

- (void)testClearData
{
    NSString* key = @"testKey";
    NSDictionary* testDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:15], key, nil];
    [testData addData:testDictionary];
    
    [testData clearData];
    XCTAssertNil(testData.queryResults);
}

@end
