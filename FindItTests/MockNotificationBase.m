//
//  MockNotification.m
//  FindIt
//
//  Created by Philip Holst on 10/5/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "MockNotificationBase.h"

@implementation MockNotificationBase

- (id)init
{
    self = [super init];
    if (self)
    {
        _wasHit = NO;
    }
    return self;
}

- (void)mockNotification:(NSNotification *)notification
{
    self.wasHit = YES;
}

- (void)tearDown
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
