//
//  UserTypeConverter.h
//  FindIt
//
//  Created by Philip Holst on 10/7/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceDefines.h"

// Class of convienence conversion functions.

@interface UserTypeConverter : NSObject

+ (NSString*)convertWebServiceToString:(WebServiceType)webService;
+ (WebServiceType)convertStringtoWebServiceType:(NSString*)string;

+ (NSString*)convertLocationTypeToString:(LocationType)location forService:(WebServiceType)webService;
+ (LocationType)convertStringToLocationType:(NSString*)string forService:(WebServiceType)webService;

+ (NSString*)getDefaultLocationTypeString;
+ (WebServiceType)getDefaultWebService;
+ (LocationType)getDefaultLocationType;

+ (NSString*)getLocationDisplayName:(LocationType)location;

@end
