//
//  CustomInfoWindowView.m
//  FindIt
//
//  Created by Philip Holst on 10/7/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "CustomInfoWindowView.h"

// There is no special code here. This is a container class, and all of the hard work is done by
// the generated properties or the super class. 

@implementation CustomInfoWindowView

@end
