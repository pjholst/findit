//
//  LocationInterfaceTests.m
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

// TODO: Convert these test to OCTest.

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <objc/runtime.h>

#import "LocationInterface.h"
#import "MockNotificationBase.h"
#import "NotificationDefines.h"

#pragma mark - MockResponder
@interface MockResponder : MockNotificationBase

@property double latitude;
@property double longitude;

@end

@implementation MockResponder

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mockNotification:) name:LocationUpdateNotification object:nil];
    }
    
    return self;
}

- (void)mockNotification:(NSNotification *)notification
{
    [super mockNotification:notification];
    NSNumber* latNumber = [notification.userInfo objectForKey:kLatitudeKey];
    NSNumber* longNumber = [notification.userInfo objectForKey:kLongitudeKey];
    self.latitude = [latNumber doubleValue];
    self.longitude = [longNumber doubleValue];
}

@end

#pragma mark - UIAlertView Swizzle

static NSString* errorMessage = nil;

@interface UIAlertView (UnitTesting)

- (void)sizzleShow;

@end

@implementation UIAlertView (UnitTesting)

- (void)sizzleShow
{
    errorMessage = self.message;
}

@end

#pragma mark - LocationInterfaceTests

@interface LocationInterfaceTests : XCTestCase
{
    LocationInterface* testInterface;
    MockResponder* mockResponder;
}

@end

@implementation LocationInterfaceTests

- (void)setUp {
    [super setUp];
    testInterface = [[LocationInterface alloc] init];
    mockResponder = [[MockResponder alloc] init];
}

- (void)tearDown {
    [mockResponder tearDown];
    [super tearDown];
}

- (void)testThatDidUpdateLocationsFilterBadLocations
{
    CLLocation* testLocation = [[CLLocation alloc] initWithLatitude:0 longitude:0];
    [testInterface locationManager:nil didUpdateLocations:@[testLocation]];
    
    XCTAssertFalse(mockResponder.wasHit, @"mockResponder should not be hit");
}

- (void)testThatDidUpdateLocationsSendsNotificationForGoodData
{
    CLLocation* testLocation = [[CLLocation alloc] initWithLatitude:123 longitude:456];
    [testInterface locationManager:nil didUpdateLocations:@[testLocation]];
    
    XCTAssert(mockResponder.wasHit, @"mockResponder should be hit");
    XCTAssertEqual(123, mockResponder.latitude, @"Latitude does not match");
    XCTAssertEqual(456, mockResponder.longitude, @"Longitude does not match");
}

- (void)testThatDidFailWithErrorWorks
{
    Method orginalMethod = class_getInstanceMethod([UIAlertView class], @selector(show));
    Method swizzleMethod = class_getInstanceMethod([UIAlertView class], @selector(sizzleShow));
    
    method_exchangeImplementations(orginalMethod, swizzleMethod);
   
    {
        NSError* error = [[NSError alloc] initWithDomain:kCLErrorDomain code:kCLErrorLocationUnknown userInfo:nil];
        [testInterface locationManager:nil didFailWithError:error];
        XCTAssertEqual(NSLocalizedString(@"Location Unknown", nil), errorMessage);
    }
    
    {
        NSError* error = [[NSError alloc] initWithDomain:kCLErrorDomain code:kCLErrorDenied userInfo:nil];
        [testInterface locationManager:nil didFailWithError:error];
        XCTAssertEqual(NSLocalizedString(@"Location Services Denied", nil), errorMessage);
    }
    
    {
        NSError* error = [[NSError alloc] initWithDomain:kCLErrorDomain code:kCLErrorNetwork userInfo:nil];
        [testInterface locationManager:nil didFailWithError:error];
        XCTAssertEqual(NSLocalizedString(@"Location Services Network Error", nil), errorMessage);
    }
    
    {
        // This is just one of the errors we don't explicitly handle.
        
        NSError* error = [[NSError alloc] initWithDomain:kCLErrorDomain code:kCLErrorHeadingFailure userInfo:nil];
        [testInterface locationManager:nil didFailWithError:error];
        XCTAssertEqual(NSLocalizedString(@"Location Services could not find your location", nil), errorMessage);
    }
    
    method_exchangeImplementations(swizzleMethod, orginalMethod);
}

@end
