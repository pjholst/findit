//
//  QueryData.m
//  FindIt
//
//  Created by Philip Holst on 10/5/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "QueryData.h"

@implementation QueryData

static QueryData* qData = nil;
+ (id)sharedData
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        qData = [[[QueryData class] alloc] init];
    });
    return qData;
}

- (void)addData:(NSDictionary *)newData
{
    self.queryResults = newData;
}

- (void)clearData
{
    self.queryResults = nil;
}

@end
