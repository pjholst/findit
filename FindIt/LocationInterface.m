//
//  LocationInterface.m
//  FindIt
//
//  Created by Philip Holst on 10/2/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "LocationInterface.h"

#import "NotificationDefines.h"

#import <UIKit/UIKit.h>

@implementation LocationInterface

- (id)init
{
    self = [super init];
    if (self)
    {
        _locationManager = [[CLLocationManager alloc] init];
        [_locationManager requestWhenInUseAuthorization];
        _locationManager.delegate = self;
    }
    return self;
}

static LocationInterface* interface = nil;

+ (id)sharedInterface
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        interface = [[[LocationInterface class] alloc] init];
    });
    return interface;
}

- (CLLocationCoordinate2D)currentCoordinate
{
    return self.locationManager.location.coordinate;
}

- (void)startLocationServices
{
    _locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    _locationManager.distanceFilter = 500; // meters
    [_locationManager startUpdatingLocation];
}

- (void)stopLocationServices
{
    [self.locationManager stopUpdatingLocation];
}

- (BOOL)hasLocation
{
    if (self.locationManager.location)
        return YES;
    else
        return NO;
}

#pragma mark - CLLocationManagerDelegate

// Consider and NSNotificationCenter notification for signalling when location has changed.
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    
    if (location.coordinate.longitude != 0 && location.coordinate.latitude != 0)
    {
        NSMutableDictionary* notificationDictionary = [[NSMutableDictionary alloc] init];
        [notificationDictionary setValue:[NSNumber numberWithDouble:location.coordinate.latitude] forKey:kLatitudeKey];
        [notificationDictionary setValue:[NSNumber numberWithDouble:location.coordinate.longitude] forKey:kLongitudeKey];

        [[NSNotificationCenter defaultCenter] postNotificationName:LocationUpdateNotification object:self userInfo:notificationDictionary];
        
        [self stopLocationServices];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error.domain isEqualToString:kCLErrorDomain])
    {
        NSString* errorStr = @"";
        BOOL shouldShowErrorNow = YES;
        CLError errorCode = error.code;
        switch (errorCode)
        {
            case kCLErrorLocationUnknown:
                errorStr = NSLocalizedString(@"Location Unknown", nil);
                //shouldShowErrorNow = NO;
                break;
                
            case kCLErrorDenied:
                errorStr = NSLocalizedString(@"Location Services Denied", nil);
                break;
                
            case kCLErrorNetwork:
                errorStr = NSLocalizedString(@"Location Services Network Error", nil);
                break;
                
            default:
                errorStr = NSLocalizedString(@"Location Services could not find your location", nil);
                break;
        }
        if (shouldShowErrorNow)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Error", nil)
                                                            message:errorStr
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
            [alertView show];
        }
        
    }
    
    NSLog(@"Hit and error %@\n", error.description);
}

@end
