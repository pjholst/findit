//
//  GoogleMapView.m
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "GoogleMapView.h"

#import "CustomInfoWindowView.h"
#import "GeneralMapView.h"
#import "GMSCoordinateBoundsAdditions.h"
#import "PlaceData.h"
#import "QueryData.h"
#import "WebServiceInterface.h"

static NSString* const kUserLocationStr = @"userLocation";

@implementation GoogleMapView

- (id)initWithFrameForMap:(CGRect)frame
{
    self = [super init];
    if (self)
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                                longitude:151.20
                                                                     zoom:6];
        mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
        mapView_.mapType = kGMSTypeNormal;
        mapView_.delegate = self;
        
        markerArray_ = [NSArray array];
    }
    return self;
}

- (void)dealloc
{
    [self clearMarkers];
}

#pragma mark - GeneralMapViewProtocol Methods

- (UIView*)getView
{
    return mapView_;
}

- (void)resetFrame:(CGRect)frame
{
    mapView_.frame = frame;
}

- (void)setUserPosiiton:(CLLocationCoordinate2D)coordinate
{
    if (userPositionMarker_ == nil)
        userPositionMarker_ = [[GMSMarker alloc] init]; 
    
    userPositionMarker_.position = coordinate;
    userPositionMarker_.title = NSLocalizedString(@"Current Location", nil);
    userPositionMarker_.userData = kUserLocationStr;
    userPositionMarker_.icon = [GeneralMapView colorCircle:[UIColor redColor]];
    userPositionMarker_.map = mapView_;
}

- (void)setCenterPosition:(CLLocationCoordinate2D)coordinate
{
    GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:coordinate];
    [mapView_ moveCamera:cameraUpdate];
}

- (void)setZoomForRadius:(double)radius aroundCoordinate:(CLLocationCoordinate2D)coordinate
{
    GMSCoordinateBounds* bounds = [GMSCoordinateBounds GMSCoordinateBoundsWithDistance:radius
                                                                      aroundCoordinate:coordinate];
    GMSCameraUpdate* cameraUpdate = [GMSCameraUpdate fitBounds:bounds];
    [mapView_ moveCamera:cameraUpdate];
}

- (void)addMarkersFromPlaceData
{
    NSMutableArray* tempArray = [NSMutableArray array];
    NSDictionary* query = [[QueryData sharedData] queryResults];
    for (NSString* key in query)
    {
        PlaceData* placeData = [query objectForKey:key];
        GMSMarker* marker = [GMSMarker markerWithPosition:placeData.coordinate];
        marker.title = placeData.name;
        marker.userData = key;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        
        marker.map = mapView_;
        
        [tempArray addObject:marker];
    }
    markerArray_ = [markerArray_ arrayByAddingObjectsFromArray:tempArray];
}

- (void)clearMarkers
{
    if (markerArray_.count > 0)
    {
        for (GMSMarker* marker in markerArray_)
        {
            marker.map = nil;
        }
        markerArray_ = [NSArray array];
    }
}

- (void)addRangeRing:(double)radius aroundCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (!rangeCircle_)
        rangeCircle_ = [GMSCircle circleWithPosition:coordinate radius:radius];
    
    // add color information here;
    rangeCircle_.map = mapView_;
}

- (void)changeRangeRingColor:(UIColor *)color
{
    if (rangeCircle_)
        rangeCircle_.strokeColor = color;
}

- (void)removeRangeRing
{
    rangeCircle_.map = nil;
    rangeCircle_ = nil;
    
    // The following is to work around an oddity of GMSCircle.
    // Unlike other GMSOverlay objects, Circles don't get removed from the map
    // by removing their map property. In order to remove it, we must clear the
    // entire map, then re-add anything that was there before.
    [mapView_ clear];
    if (userPositionMarker_)
        userPositionMarker_.map = mapView_;
    
    for (GMSMarker* placeMarker in markerArray_)
    {
        placeMarker.map = mapView_;
    }
}

#pragma mark - GMSMapViewDelegate
- (UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    if (marker.userData == kUserLocationStr)
    {
        // Returning nil causes the default maker info window to pop. This is ok for the current location.
        return nil;
    }
    else
    {
        PlaceData* data = [[[QueryData sharedData] queryResults] objectForKey:marker.userData];
        CustomInfoWindowView* infoWindow = [[[NSBundle mainBundle] loadNibNamed:@"CustomInfoWindow" owner:self options:nil] objectAtIndex:0];

        infoWindow.nameLabel.text = data.name;
        infoWindow.addressLabel.text = data.address;
        UIImage* iconImage = [UIImage imageWithData:[[WebServiceInterface sharedInterface] fetchDataFromURL:data.imageIcon]];
        if (iconImage)
            infoWindow.imageView.image = iconImage;
        
        return infoWindow;
    }
}

@end
