//
//  PrefsViewController.m
//  FindIt
//
//  Created by Philip Holst on 10/6/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "PrefsViewController.h"
#import "UserTypeConverter.h"
#import "WebServiceDefines.h"
#import "UserDefaultPrefsDefines.h"

@interface PrefsViewController ()

@end

@implementation PrefsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupWebServiceNames];
    self.webServicePicker.dataSource = self;
    self.webServicePicker.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSNumber* radiusNumber = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultRadius];
    self.rangeSlider.value = [radiusNumber floatValue] * METERS_TO_MILES;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:1];
    self.rangeLabel.text = [NSString stringWithFormat:@"%@ Miles",
                            [formatter stringFromNumber:[NSNumber numberWithDouble:[radiusNumber floatValue] * METERS_TO_MILES]]];
    
    self.rangeRingSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:kDefaultRangeRing];
}

- (void)viewWillDisappear:(BOOL)animated
{
    NSString* webServiceString = [UserTypeConverter convertWebServiceToString:(WebServiceType)[self.webServicePicker selectedRowInComponent:0]];
    [[NSUserDefaults standardUserDefaults] setObject:webServiceString forKey:kDefaultService];
    [[NSUserDefaults standardUserDefaults] setDouble:self.rangeSlider.value * MILES_TO_METER forKey:kDefaultRadius];
    [[NSUserDefaults standardUserDefaults] setBool:self.rangeRingSwitch.on forKey:kDefaultRangeRing];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*)getDisplayName:(WebServiceType)service
{
    NSString* returnStr = nil;
    switch (service) {
        case kGoogleService:
            returnStr = NSLocalizedString(@"Google", nil);
            break;
            
        case kYelpService:
            returnStr = NSLocalizedString(@"Yelp", nil);
            break;
            
            // Add more web services here.
            
        default:
            [NSException raise:NSGenericException format:@"Unexpected WebService."];
            break;
    }
    return returnStr;
}

- (void)setupWebServiceNames
{
    NSMutableArray* tempArray = [[NSMutableArray alloc] init];
    for (WebServiceType service = 0 /*should be Google*/; service < kLastService; service++)
    {
        [tempArray addObject:[self getDisplayName:service]];
    }
    self.webServiceNames = [NSArray arrayWithArray:tempArray];
}

- (IBAction)rangerSliderUpdated:(id)sender
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:1];
    self.rangeLabel.text = [NSString stringWithFormat:@"%@ Miles",
                           [formatter stringFromNumber:[NSNumber numberWithFloat:self.rangeSlider.value]]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIPickerViewDelegate

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.webServiceNames objectAtIndex:row];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return kLastService;
}

@end
