//
//  WebServiceInterfaceProtocol.h
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceDefines.h"

@protocol WebServiceInterfaceProtocol

    @required
    - (void)startQueryForLocationType:(LocationType)locationType withRadius:(double)radius;
    - (BOOL)processResponseData:(NSData*)data;
    - (void)processError:(NSError*)error;

@end
