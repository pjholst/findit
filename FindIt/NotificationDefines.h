//
//  NotificationDefines.h
//  FindIt
//
//  Created by Philip Holst on 10/2/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//


#import <Foundation/Foundation.h>

//
// Notification Names
//

// General

// Notification is fired when Location is found by Location Services. Two Doubles are passed using keys kLatitudeKey and kLongitudeKey.
static NSString * const LocationUpdateNotification  = @"LocationUpdateNotification";

// Notification is fired when new location data is aquire from web service. No data is passed.
static NSString * const NewPlaceDataNotification    = @"NewPlaceDataNotification";

// Pref Notifications

// Notification is fired when the location type pref has been changed. No data is passed.
static NSString * const LocationTypePrefNotification = @"UpdateLocationPrefNotification";

// Notification is fired when the range/radius pref has been changed. No data is passed.
static NSString * const RangePrefNotification        = @"UpdateRangePrefNotification";

// Notification is fired when the range ring pref has been changed. A BOOL is passed with key kRangeChangeKey.
static NSString * const RangeRingPrefNotification    = @"UpdateRangeRingPrefNotification";

//
// Dictionary Keys
//
static NSString * const kLatitudeKey    = @"Latitude";
static NSString * const kLongitudeKey   = @"Longitude";
static NSString * const kRangeChangeKey = @"RangeChange";
