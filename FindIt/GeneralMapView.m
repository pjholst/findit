//
//  GeneralMapView.m
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "GeneralMapView.h"

#import "GoogleMapView.h"
#import "LocationInterface.h"
#import "NotificationDefines.h"
#import "UserDefaultPrefsDefines.h"

@implementation GeneralMapView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // If MapKit support is added, this will need to be updated. See the init for
        // WebServiceInterface for pattern.
        _mapDelegate = [[GoogleMapView alloc] init];
        [self addSubview:[_mapDelegate getView]];
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// This setup must be done after the view has been brought to the screen. If we don't wait, the bounds of this
// view will not reflect what it is on the active device, and will only show up as the default size.
- (void)setupMap
{
    if (!self.isHidden)
    {
        // Setup the map with the frame to match this view's bounds.
        self.mapDelegate = [[GoogleMapView alloc] initWithFrameForMap:self.bounds];
        [self addSubview:[self.mapDelegate getView]];
        
        // Setup the observers here.
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(newPlaceData:)
                                                     name:NewPlaceDataNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(rangeRingPrefChanged:)
                                                     name:RangeRingPrefNotification
                                                   object:nil];
    }
    
}

- (void)clearMap
{
    [[self.mapDelegate getView] removeFromSuperview];
    self.mapDelegate = nil;
}

- (void)resetMapFrame
{
    [self.mapDelegate resetFrame:self.bounds];
}

- (void)setUserPosition:(CLLocationCoordinate2D)coordinate
{
    NSAssert(self.mapDelegate, @"mapDelgate should not be nil");
    if (self.mapDelegate)
    {
        double range = [[NSUserDefaults standardUserDefaults] doubleForKey:kDefaultRadius];
        [self.mapDelegate setUserPosiiton:coordinate];
        [self.mapDelegate setCenterPosition:coordinate];
        [self.mapDelegate setZoomForRadius:range aroundCoordinate:coordinate];

        if ([[NSUserDefaults standardUserDefaults] boolForKey:kDefaultRangeRing])
        {
            [self addRangeRing:range atCoordinate:coordinate];
        }
    }
}

- (void)addRangeRing:(double)radius atCoordinate:(CLLocationCoordinate2D)coordinate
{
    // This is a call to an optional protocol method
    if (self.mapDelegate && [self.mapDelegate respondsToSelector:@selector(addRangeRing:aroundCoordinate:)])
        [self.mapDelegate addRangeRing:radius aroundCoordinate:coordinate];
}

- (void)changeRangeRingColor:(UIColor *)color
{
    // This is a call to an optional protocol method
    if (self.mapDelegate && [self.mapDelegate respondsToSelector:@selector(changeRangeRingColor:)])
    {
        [self.mapDelegate changeRangeRingColor:color];
    }
}

- (void)removeRangeRing
{
    // This is a call to an optional protocol method
    if (self.mapDelegate && [self.mapDelegate respondsToSelector:@selector(removeRangeRing)])
    {
        [self.mapDelegate removeRangeRing];
    }
}

- (void)clearMarkers
{
    if (self.mapDelegate)
        [self.mapDelegate clearMarkers];
}

- (void)newPlaceData:(NSNotification *)notification
{
    // As this notification is likely to come from the non-main queue, and that the results
    // will likely affect the UI, dispatch to the main queue now.
    dispatch_async(dispatch_get_main_queue(), ^(){
        [self.mapDelegate clearMarkers];
        [self.mapDelegate addMarkersFromPlaceData];
    });
}

- (void)rangeRingPrefChanged:(NSNotification *)notification
{
    // As this notification is likely to come from the non-main queue, and that the results
    // will likely affect the UI, dispatch to the main queue now.

    BOOL rangeChanged = [(NSNumber*)[notification.userInfo objectForKey:kRangeChangeKey] boolValue];
    dispatch_async(dispatch_get_main_queue(), ^(){
        BOOL hasLocation = [[LocationInterface sharedInterface] hasLocation];
        BOOL ringOn = [[NSUserDefaults standardUserDefaults] boolForKey:kDefaultRangeRing];
        if (hasLocation && ringOn)
        {
            // If the range changed, remove the previous ring before adding a new one.
            if (rangeChanged)
                [self removeRangeRing];
                
            [self addRangeRing:[[NSUserDefaults standardUserDefaults] doubleForKey:kDefaultRadius]
                  atCoordinate:[[LocationInterface sharedInterface] currentCoordinate]];
        }
        else if (hasLocation && !ringOn)
        {
            [self removeRangeRing];
        }
    });
}


// Method to make a small circle. Based on code from StackOverflow.
+ (UIImage *)colorCircle:(UIColor*)color
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(10.f, 10.f), NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGRect rect = CGRectMake(0, 0, 10, 10);
    CGContextSetFillColorWithColor(ctx, color.CGColor);
    CGContextFillEllipseInRect(ctx, rect);
    
    CGContextRestoreGState(ctx);
    UIImage *colorCircle = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return colorCircle;
}

@end
