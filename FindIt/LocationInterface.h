//
//  LocationInterface.h
//  FindIt
//
//  Created by Philip Holst on 10/2/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

// Light wrapper class around CLLocationManager. This interface is a single interface
// point for starting and stopping the location services on the device.
// Also provides easy access to the current location coordinates. 
@interface LocationInterface : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager* locationManager;

+ (id)sharedInterface;
- (CLLocationCoordinate2D)currentCoordinate;
- (void)startLocationServices;
- (void)stopLocationServices;
- (BOOL)hasLocation;

@end
