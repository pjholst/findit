//
//  GMSCoordinateBoundsAdditions.m
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "GMSCoordinateBoundsAdditions.h"
#import <MapKit/MapKit.h>

@implementation GMSCoordinateBounds (DistanceInitAdditions)

// Creates a bounds around a coordinate. 
+ (GMSCoordinateBounds*)GMSCoordinateBoundsWithDistance:(double)distance aroundCoordinate:(CLLocationCoordinate2D)coordinate
{
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, distance, distance);
    CLLocationCoordinate2D topRight = CLLocationCoordinate2DMake(coordinate.latitude + viewRegion.span.latitudeDelta, coordinate.longitude + viewRegion.span.longitudeDelta);
    CLLocationCoordinate2D bottomLeft = CLLocationCoordinate2DMake(coordinate.latitude - viewRegion.span.latitudeDelta, coordinate.longitude - viewRegion.span.longitudeDelta);
    
    GMSCoordinateBounds* bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:topRight coordinate:bottomLeft];
    return bounds;
}

@end
