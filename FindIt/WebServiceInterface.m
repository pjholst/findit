//
//  WebServiceInterface.m
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "WebServiceInterface.h"

#import "GooglePlacesInterface.h"
#import "NotificationDefines.h"
#import "UserDefaultPrefsDefines.h"
#import "UserTypeConverter.h"

@implementation WebServiceInterface

- (id)init
{
    self = [super init];
    if (self)
    {
        _webType = [UserTypeConverter convertStringtoWebServiceType:[[NSUserDefaults standardUserDefaults] stringForKey:kDefaultService]];
        _delegate = [[[self getWebServiceClass] alloc] init];
        _queue = [[NSOperationQueue alloc] init];
    }
    return self;
}

static WebServiceInterface* interface = nil;
+ (id)sharedInterface
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        interface = [[[WebServiceInterface class] alloc] init];
    });
    return interface;
}

- (Class)getWebServiceClass
{
    Class interfaceClass;
    
    switch (_webType) {
        case kGoogleService:
            interfaceClass = [GooglePlacesInterface class];
            break;
            
        case kYelpService:
            [NSException raise:NSGenericException format:@"Unexpected WebService."];
            break;
            
        case kLastService:
            [NSException raise:NSGenericException format:@"Unexpected WebService."];
            break;
    }
    
    return interfaceClass;
}

- (void)startWebQuery
{
    if (self.delegate)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        LocationType locationType = [UserTypeConverter convertStringToLocationType:[defaults stringForKey:kDefaultLocationType]
                                                                        forService:self.webType];
        double radius = [defaults doubleForKey:kDefaultRadius];
        
        [self.delegate startQueryForLocationType:locationType withRadius:radius];
    }
}

- (void)processHTTPRequest:(NSURL *)url
{
    // Convert the url to an NURLRequest. Then send it asychronusly. The response queue is a non-main queue.
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:self.queue
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             if ([self.delegate processResponseData:data])
                 [[NSNotificationCenter defaultCenter] postNotificationName:NewPlaceDataNotification object:self];
         }
         else if (connectionError)
         {
             [self.delegate processError:connectionError];
             NSLog(@"Connection Error: %@", connectionError.debugDescription);
         }
     }];
}

- (NSData*)fetchDataFromURL:(NSURL*)url
{
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData* returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    return returnData;
}

@end
