//
//  GMSCoordinateBoundsAdditions.h
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface GMSCoordinateBounds (DistanceInitAdditions)

+ (GMSCoordinateBounds*)GMSCoordinateBoundsWithDistance:(double)distance aroundCoordinate:(CLLocationCoordinate2D)coordinate;

@end
