//
//  MapViewController.h
//  FindIt
//
//  Created by Philip Holst on 10/2/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

#import "GeneralMapView.h"
#import "LocationPicker.h"
#import "WebServiceDefines.h"


@interface MapViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet GeneralMapView *mapView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *locationPickerButton;
@property (weak, nonatomic) LocationPicker *locationPickerView;

- (IBAction)startSearch:(id)sender;
- (IBAction)centerOnMyLocation:(id)sender;
- (IBAction)selectLocationType:(id)sender;

- (void)openLocationPicker;
- (void)closeLocationPicker;

- (void)animateLoctionPickerOpen:(NSTimeInterval)duration;
- (void)animateLoctionPickerClose:(NSTimeInterval)duration;

- (void)clearMapLocations;

// Notifications
- (void)positionChanged:(NSNotification*)notification;
- (void)locationTypeChanged:(NSNotification*)notification;

@end
