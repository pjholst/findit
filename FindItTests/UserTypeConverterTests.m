//
//  UserTypeConverterTests.m
//  FindIt
//
//  Created by Philip Holst on 10/8/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "UserTypeConverter.h"

@interface UserTypeConverterTests : XCTestCase

@end

@implementation UserTypeConverterTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testConvertWebServiceToString
{
    for (WebServiceType webService = 0; webService < kLastService; webService++)
    {
        NSString* string = [UserTypeConverter convertWebServiceToString:webService];
        XCTAssertGreaterThan(string.length, 0);
    }
    
    XCTAssertThrows([UserTypeConverter convertWebServiceToString:kLastService]);
}

- (void)testConcertStringToWebService
{
    XCTAssertEqual([UserTypeConverter convertStringtoWebServiceType:kGoogleServiceString], kGoogleService);
    XCTAssertEqual([UserTypeConverter convertStringtoWebServiceType:kYelpServiceString], kYelpService);
    
    XCTAssertThrows([UserTypeConverter convertStringtoWebServiceType:@"Random String"]);
}

- (void)testConvertLocationTypeToString
{
    for (WebServiceType webService = 0; webService < kLastService; webService++)
    {
        for (LocationType location = 0; location <= kLastLocation; location++)
        {
            if (location != kLastLocation)
                XCTAssertGreaterThan([[UserTypeConverter convertLocationTypeToString:location forService:webService] length], 0);
            else
                XCTAssertThrows([UserTypeConverter convertLocationTypeToString:location forService:webService]);
        }
    }
    
    XCTAssertThrows([UserTypeConverter convertLocationTypeToString:kLastLocation forService:kLastService]);
}

- (void)testConvertStringToLocationGoogle
{
    XCTAssertEqual([UserTypeConverter convertStringToLocationType:kGoogleLocationFlorist forService:kGoogleService], kFlorist);
    XCTAssertEqual([UserTypeConverter convertStringToLocationType:kGoogleLocationRestaurant forService:kGoogleService], kRestaurant);
    XCTAssertEqual([UserTypeConverter convertStringToLocationType:kGoogleLocationGasStation forService:kGoogleService], kGasStation);
    
    // Pass a random string just to make sure it is not a match.
    XCTAssertThrows([UserTypeConverter convertStringToLocationType:@"Random" forService:kGoogleService]);
}

- (void)testConvertStringToLocationYelp
{
    XCTAssertEqual([UserTypeConverter convertStringToLocationType:kYelpLocationFlorist forService:kYelpService], kFlorist);
    XCTAssertEqual([UserTypeConverter convertStringToLocationType:kYelpLocationRestaurant forService:kYelpService], kRestaurant);
    XCTAssertEqual([UserTypeConverter convertStringToLocationType:kYelpLocationGasStation forService:kYelpService], kGasStation);
    
    // Pass a random string just to make sure it is not a match.
    XCTAssertThrows([UserTypeConverter convertStringToLocationType:@"Random" forService:kYelpService]);
}

- (void)testConvertStringToLocationLastService
{
    // The String should not matter.
    XCTAssertThrows([UserTypeConverter convertStringToLocationType:@"random" forService:kLastService]);
}




@end
