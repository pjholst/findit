//
//  QueryData.h
//  FindIt
//
//  Created by Philip Holst on 10/5/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>

// Light container class to hold the results from a web query. It is a singleton to make sharing
// of data a little bit eaiser between the data service layer and the UI / Map layer. 
@interface QueryData : NSObject

// The unique id of each record from the resulting query should be used as the for the records in this dictionary.
@property (strong, nonatomic) NSDictionary* queryResults;
@property (nonatomic) BOOL hasMoreResults;
@property (copy, nonatomic) NSString* moreResultsKey;

+ (id)sharedData;
- (void)addData:(NSDictionary*)newData;
- (void)clearData;

@end
