//
//  CustomInfoWindowView.h
//  FindIt
//
//  Created by Philip Holst on 10/7/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>

// Custom information view for pop up views on map.
@interface CustomInfoWindowView : UIView

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
