//
//  LocationPicker.h
//  FindIt
//
//  Created by Philip Holst on 10/7/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationPicker : UIView

@property (weak, nonatomic) IBOutlet UIPickerView *locationPicker;
@property (weak, nonatomic) IBOutlet UISlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *rangeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *rangeRingSwitch;

- (void)setupView;
- (void)setPrefsOnClose;

- (IBAction)rangerSliderUpdated:(id)sender;

@end
