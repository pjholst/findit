//
//  GeneralMapView.h
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeneralMapViewProtocol.h"

@interface GeneralMapView : UIView

@property (strong, nonatomic) id<GeneralMapViewProtocol> mapDelegate;

- (void)setupMap;
- (void)clearMap;
- (void)resetMapFrame;
- (void)setUserPosition:(CLLocationCoordinate2D)coordinate;
- (void)addRangeRing:(double)radius atCoordinate:(CLLocationCoordinate2D)coordinate;
- (void)changeRangeRingColor:(UIColor*)color;
- (void)removeRangeRing;
- (void)clearMarkers;

// Notifications
- (void)newPlaceData:(NSNotification*)notification;
- (void)rangeRingPrefChanged:(NSNotification*)notification;

// Utility Methods
+ (UIImage *)colorCircle:(UIColor*)color;

@end
