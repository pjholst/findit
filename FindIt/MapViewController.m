//
//  MapViewController.m
//  FindIt
//
//  Created by Philip Holst on 10/2/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import "MapViewController.h"

#import "GMSCoordinateBoundsAdditions.h"
#import "LocationInterface.h"
#import "NotificationDefines.h"
#import "QueryData.h"
#import "UserDefaultPrefsDefines.h"
#import "UserTypeConverter.h"
#import "WebServiceInterface.h"


@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(positionChanged:) name:LocationUpdateNotification object:nil];
    [center addObserver:self selector:@selector(locationTypeChanged:) name:LocationTypePrefNotification object:nil];
    [center addObserver:self selector:@selector(locationTypeChanged:) name:RangePrefNotification object:nil];
    
    [self.mapView setupMap];
    LocationInterface *interface = [LocationInterface sharedInterface];
    if (![interface hasLocation])
         [interface startLocationServices];
    else
        [self.mapView setUserPosition:[interface currentCoordinate]];
    
    self.locationPickerButton.title = [UserTypeConverter getLocationDisplayName:[UserTypeConverter getDefaultLocationType]];
}

- (void)viewDidDisappear:(BOOL)animated
{
    // Clear the Map and Remove the Observers.
    [self.mapView clearMap];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
       // Nothing. 
    }
                                 completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
                                     [self.mapView resetMapFrame];
                                     [self.mapView setUserPosition:[[LocationInterface sharedInterface] currentCoordinate]];
    }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}
     
- (IBAction)startSearch:(id)sender
{
    [[WebServiceInterface sharedInterface] startWebQuery];
}

- (IBAction)centerOnMyLocation:(id)sender
{
    LocationInterface *sharedInterace = [LocationInterface sharedInterface];
    if (![sharedInterace hasLocation])
    {
        [sharedInterace startLocationServices];
    }
    else
    {
        CLLocationCoordinate2D coordinate = [sharedInterace currentCoordinate];
        [self.mapView setUserPosition:coordinate];
    }
}

- (IBAction)selectLocationType:(id)sender
{
    if (self.locationPickerView == nil)
    {
        [self openLocationPicker];
    }
    else
    {
        [self closeLocationPicker];
    }
}

- (void)openLocationPicker
{
    self.locationPickerView = [[[NSBundle mainBundle] loadNibNamed:@"LocationPicker"
                                                             owner:self options:nil]
                               objectAtIndex:0];
    
    self.locationPickerView.locationPicker.delegate = self;
    self.locationPickerView.locationPicker.dataSource = self;
    
    [self.locationPickerView setupView];
    
    // Set the frame to be centered horizontally, and just below the view vertically (so it is completely offscreen).
    CGFloat xVal = (self.view.frame.size.width - self.locationPickerView.frame.size.width) / 2.f;
    CGFloat width = self.locationPickerView.frame.size.width;
    if (xVal < 0)
    {
        xVal = 0;
        width = self.view.frame.size.width;
    }
    xVal = xVal < 0 ? 0 : xVal;
    
    CGRect startRect = CGRectMake(xVal,
                                  self.view.frame.size.height,
                                  width,
                                  self.locationPickerView.frame.size.height);
    self.locationPickerView.frame = startRect;
    [self.view addSubview:self.locationPickerView];
    self.locationPickerButton.title = @"Done";
    
    // Animate brings the view up from the bottom.
    [self animateLoctionPickerOpen:0.5];
}

- (void)closeLocationPicker
{
    [self.locationPickerView setPrefsOnClose];
    
    self.locationPickerButton.title = [UserTypeConverter getLocationDisplayName:[UserTypeConverter getDefaultLocationType]];
    
    NSTimeInterval duration = 0.5f;
    // Animate takes the view off screen.
    [self animateLoctionPickerClose:duration];
    
    // This must be called after the animation is complete, otherwise, the view just pops away.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.locationPickerView removeFromSuperview];
        self.locationPickerView = nil;
    });
}

- (void)animateLoctionPickerOpen:(NSTimeInterval)duration
{
    CGRect upFrame = self.locationPickerView.frame;
    upFrame.origin.y -= self.locationPickerView.frame.size.height + 44; // Height of the toolbar
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    self.locationPickerView.frame = upFrame;
    
    [UIView commitAnimations];
}

- (void)animateLoctionPickerClose:(NSTimeInterval)duration
{
    CGRect dnFrame = self.locationPickerView.frame;
    dnFrame.origin.y += self.locationPickerView.frame.size.height + 44; // Height of the toolbar
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    self.locationPickerView.frame = dnFrame;
    
    [UIView commitAnimations];
}

- (void)clearMapLocations
{
    [self.mapView clearMarkers];
    [[QueryData sharedData] clearData];
}

- (void)positionChanged:(NSNotification *)notification
{
    NSNumber* latNumber = [notification.userInfo objectForKey:kLatitudeKey];
    NSNumber* longNumber = [notification.userInfo objectForKey:kLongitudeKey];
    if (latNumber && longNumber)
    {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([latNumber doubleValue], [longNumber doubleValue]);
        
        [self.mapView setUserPosition:coordinate];
    }
}

- (void)locationTypeChanged:(NSNotification *)notification
{
    [self clearMapLocations];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return kLastLocation;
}

#pragma mark - UIPickerViewDelegate
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [UserTypeConverter getLocationDisplayName:(LocationType)row];
}

#pragma mark - Navigation

// The following may be needed if more map frameworks are added (ie MapKit), or more WebServices (ie Yelp).
// These additions would require a change in our the UI works, which would likely require new views in the Storyboard.

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}


@end
