//
//  WebServiceDefines.h
//  FindIt
//
//  Created by Philip Holst on 10/3/14.
//  Copyright (c) 2014 HolstStuff. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    kGoogleService = 0,
    kYelpService,
    
    kLastService // Unused. Only for looping through all services.
}
WebServiceType;

static NSString* const kGoogleServiceString = @"GOOGLE";
static NSString* const kYelpServiceString = @"YELP";

typedef enum
{
    kFlorist = 0,
    kRestaurant,
    kGasStation,
    
    kLastLocation // For interation purposes only.
}
LocationType;

// Google Strings for LocationType
// See https://developers.google.com/places/documentation/supported_types
static NSString* const kGoogleLocationFlorist       = @"florist";
static NSString* const kGoogleLocationRestaurant    = @"restaurant";
static NSString* const kGoogleLocationGasStation    = @"gas_station";

// Yelp Strings for LocationType
// See http://www.yelp.com/developers/documentation/v2/all_category_list
static NSString* const kYelpLocationFlorist         = @"florists";
static NSString* const kYelpLocationRestaurant      = @"restaurants";
static NSString* const kYelpLocationGasStation      = @"servicestations";

